package com.example.tefa.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.viewpager.widget.PagerAdapter
import com.example.tefa.R

class BannerSliderAdapter(
    private val itemList: List<Int>
) : PagerAdapter() {
    override fun getCount(): Int {
        return itemList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val view = LayoutInflater.from(container.context).inflate(
            R.layout.item_banner_home, container, false
        )

        with(view) {
            this.findViewById<ImageView>(R.id.ivItemBanner).setImageDrawable(
                AppCompatResources.getDrawable(
                    context, itemList[position]
                )
            )
        }

        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}

